#!/bin/sh

mkdir -p ~/log

dateTime="metrics_"$(date +"%Y%m%d%H%M%S")
ramUsage=$(free -m | awk 'BEGIN{} {for (i=2; i<=NF; i++) {if (NR != 1) printf("%s,", $i);}}' >> /home/noxclow/log/$dateTime.log)
directorySize=$(du -sh /home/noxclow | awk '{printf $2 ", " $1}' >> /home/noxclow/log/$dateTime.log)

chmod 400 /home/noxclow/log/metrics_$dateTime.log
