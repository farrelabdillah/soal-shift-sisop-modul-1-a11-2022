#!/bin/bash

search=/home/farrel/soal2
folder=$search/forensic_log_website_daffainfo_log
logfile=$search/log_website_daffainfo.log

#sudo bash soal2_forensic_dapos.sh

mkdir $folder

cat $logfile | awk ' 
BEGIN{FS=":"}
	{gsub(/"/, "", $3)
	arr[$3]++}
	END {
		for (i in arr) {
			if (i != "Request"){
				n+=arr[i]
			}
		}
		avg=n/12 
		printf "rata rata serangan perjam adalah sebanyak %f request per jam", avg}
' >> $folder/ratarata.txt
#rata-rata dibagi 12 karena log yang tertera berjangka waktu 12 jam

cat $logfile | awk '
BEGIN{FS=":"}
	{gsub(/"/, "", $1)
	arr[$1]++}
	END {
		ip
		total=0
		for (i in arr){
			if (total < arr[i]){
				ip = i
				total = arr[ip]
			}
		}
		print "yang paling banyak mengakses server adalah: " ip " sebanyak " total " request\n"}
' >> $folder/result.txt

cat $logfile | awk '
BEGIN{FS=":"}
	/curl/ {c++} 
	END {print "ada " c " request yang menggunakan curl sebagai user-agent\n"}
' >> $folder/result.txt

cat $logfile | awk '
BEGIN{FS=":"}
	{if($3=="02") printf "%s Jam 2 Pagi \n", $1}
' >> $folder/result.txt
